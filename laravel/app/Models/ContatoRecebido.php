<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContatoRecebido extends Model
{
    protected $table = 'contato_recebido';

    protected $guarded = ['id'];
}
