<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parcerias extends Model
{
    protected $table = 'parcerias';

    protected $guarded = ['id'];
}
