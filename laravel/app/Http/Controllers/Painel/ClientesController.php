<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Helpers\Tools;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Clientes;

class ClientesController extends Controller
{
    public function index()
    {
        $registro = Clientes::first();
        return view('painel.clientes.edit', compact('registro'));
    }

    public function update(Request $request, Clientes $registro)
    {
        try{
            $input = $request->all();
            
            if(isset($input['carrossel'])){                
                $imagem = $input['carrossel'];
                foreach($imagem as $key => $item){
                    if(empty($item['foto'])){
                        $item['foto'] = $item['isFile'] ?: null;
                    }else{
                        $item['foto'] = Tools::fileUpload($item['foto'],'clientes/');
                    }
                    $imagem[$key]['foto'] = $item['foto'];
                    unset($imagem[$key]['isFile']);
                }
                $input['imagem'] = json_encode($imagem);
            }
            
            $registro->first()->update(['imagem' => $input['imagem']]);
            return redirect()->route('painel.clientes.index')->with(['success', 'Registro atualizado']);
        }catch(\Exception $e){
            return back()->withErrors('Erro ao atualizar registro.',404);
        }
    }
}
