<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Parcerias;
use App\Helpers\Tools;

class ParceriasController extends Controller
{
    public function index()
    {
        $registro = Parcerias::first();
        return view('painel.parcerias.edit', compact('registro'));
    }

    public function update(Request $request, Parcerias $registro)
    {
        try{
            $input = $request->all();

            if(isset($input['carrossel'])){                
                $parcerias = $input['carrossel'];
                foreach($parcerias as $key => $item){
                    if(empty($item['foto'])){
                        $item['foto'] = $item['isFile'] ?: null;
                    }else{
                        $item['foto'] = Tools::fileUpload($item['foto'],'parcerias/');
                    }
                    $parcerias[$key]['foto'] = $item['foto'];
                    unset($parcerias[$key]['isFile']);
                }
                $input['parcerias'] = json_encode($parcerias);
            }
            unset($input['carrossel']);

            $registro->first()->update($input);

            return redirect()->route('painel.parcerias.index')->with(['success', 'Registro atualizado']);
        }catch(\Exception $e){
            return back()->withErrors('Erro ao atualizar registro.',404);
        }
    }
}
