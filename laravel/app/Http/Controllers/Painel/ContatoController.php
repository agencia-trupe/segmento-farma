<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Contato;

class ContatoController extends Controller
{
    public function index()
    {
        $registro = Contato::first();
        return view('painel.contato.index', compact('registro'));
    }

    public function update(ContatoRequest $request, Contato $registro)
    {
        try{
            $input = $request->all();
            $registro->first()->update($input);
            return redirect()->route('painel.contato.index')->with(['success', 'Registro atualizado']);
        }catch(\Exception $e){
            return back()->withErrors('Erro ao atualizar registro.',404);
        }
    }
}
