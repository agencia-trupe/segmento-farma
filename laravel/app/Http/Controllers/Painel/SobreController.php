<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Sobre;
use App\Helpers\Tools;

class SobreController extends Controller
{
    public function index()
    {
        $registro = Sobre::first();
        return view('painel.sobre.edit', compact('registro'));
    }

    public function update(Request $request, Sobre $registro)
    {
        try{
            $input = $request->all();
            if(isset($input['documento'])) $input['documento'] = Tools::fileUpload($input['documento'],'sobre/');
            $registro->first()->update($input);
            return redirect()->route('painel.sobre.index')->with(['success', 'Registro atualizado']);
        }catch(\Exception $e){
            return back()->withErrors('Erro ao atualizar registro.',404);
        }
    }
}
