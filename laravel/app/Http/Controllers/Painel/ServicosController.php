<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Servicos;

class ServicosController extends Controller
{
    public function index()
    {
        $registro = Servicos::first();
        return view('painel.servicos.edit', compact('registro'));
    }

    public function update(Request $request, Servicos $registro)
    {
        try{
            $input = $request->all();
            $registro->first()->update($input);
            return redirect()->route('painel.servicos.index')->with(['success', 'Registro atualizado']);
        }catch(\Exception $e){
            return back()->withErrors('Erro ao atualizar registro.',404);
        }
    }
}
