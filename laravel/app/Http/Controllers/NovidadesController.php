<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DocumentosRegulamentares;
use App\Models\PerguntasFrequentes;
use App\Models\InformacoesNovidades;
use App\Http\Requests;

class NovidadesController extends Controller
{
    public function index()
    {
        return view('frontend.suporte-e-novidades');
    }

    public function novidadesPaginaInterna($slug)
    {
        $item = InformacoesNovidades::where('slug', $slug)->first();
        $items = InformacoesNovidades::limit(4)->get();
        return view('frontend.informacoes-e-novidades-pagina-interna', compact('item', 'items'));
    }

    public function documentosRegulamentares()
    {
        $items = DocumentosRegulamentares::all();
        return view('frontend.documentos-regulamentares', compact('items'));
    }

    public function perguntasFrequentes()
    {
        $items = PerguntasFrequentes::all();
        return view('frontend.perguntas-frequentes', compact('items'));
    }

    public function novidades()
    {
        $items = InformacoesNovidades::all();
        return view('frontend.informacoes-e-novidades', compact('items'));
    }
}
