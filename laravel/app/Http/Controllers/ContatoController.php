<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContatoRequest;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use Illuminate\Support\Facades\Mail;

class ContatoController extends Controller
{
    public function post(ContatoRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return back()->with(['success' => 'Mensagem enviada com sucesso!'], true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email_contato) {
           return false;
        }

        Mail::send('emails.contato', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[CONTATO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
