<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Home;
use App\Models\Sobre;
use App\Models\Servicos;
use App\Models\Parcerias;
use App\Models\Clientes;
use App\Models\Contato;
use Illuminate\Http\Request;
use App\Models\PerfilEmpresa;
use App\Models\NossosServicos;
use App\Models\PoliticaDePrivacidade;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $home = Home::first();
        $sobre = Sobre::first();
        $servicos = Servicos::first();
        $parcerias = Parcerias::first();
        $clientes = Clientes::first();
        $contato = Contato::first();
        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

        return view('frontend.home', compact('home', 'verificacao', 'sobre', 'servicos', 'parcerias', 'clientes', 'contato'));
    }

    public function clienteLogin()
    {
        return view('frontend.cliente');
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }

    public function politicaDePrivacidade()
    {
        $registro = PoliticaDePrivacidade::first();
        return view('frontend.politica-de-privacidade', compact('registro'));
    }

    public function perfilEmpresa()
    {
        $registro = PerfilEmpresa::first();
        return view('frontend.perfil-empresa', compact('registro'));
    }

    public function nossosServicos()
    {
        $registro = NossosServicos::first();
        return view('frontend.nossos-servicos', compact('registro'));
    }    

}
