<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAdmin
{
    public function handle($request, Closure $next)
    {
        if (auth()->user()->tipo !== 'administracao') {
            return redirect()->route('painel');
        }

        return $next($request);
    }
}
