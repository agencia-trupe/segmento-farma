<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('/contato', 'ContatoController@post')->name('send-form');

    Route::get('politica-de-privacidade', 'HomeController@politicaDePrivacidade')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        Route::group(['middleware' => 'admin'], function() {
            /* GENERATED ROUTES */
            Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
            Route::resource('contato', 'ContatoController', ['only' => ['index', 'update']]);
            Route::resource('parcerias', 'ParceriasController', ['only' => ['index', 'update']]);
            Route::resource('sobre', 'SobreController', ['only' => ['index', 'update']]);
            Route::resource('servicos', 'ServicosController', ['only' => ['index', 'update']]);
            Route::resource('clientes', 'ClientesController', ['only' => ['index', 'update']]);

            Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
            Route::resource('usuarios', 'UsuariosController');
            Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
            Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

            Route::post('ckeditor-upload', 'PainelController@imageUpload');
            Route::post('order', 'PainelController@order');
            Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

            Route::get('generator', 'GeneratorController@index')->name('generator.index');
            Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
