@extends('frontend.common.template')

@section('content')
<div class="politica-de-privacidade">
    <section class="container-fluid container-article">
        <article class="row-grid">            
            <div class="col-12 text-left">
                <h1>Política de Privacidade</h1>
                {!! $registro->texto !!}
            </div>
        </article>
    </section>
</div>
@endsection
