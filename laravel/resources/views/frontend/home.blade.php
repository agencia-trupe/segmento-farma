@extends('frontend.common.template')

@section('content')
    <div class="container home">
        <div class="container-banner">
            <div class="banner" style="height:100vh; background-size:cover !important; background-repeat: no-repeat !important; background: url({{ asset('assets/img/home/'.$home->banner) }}) ">
                <div class="container-fluid">
                    <div class="banner-box text-center">
                        <img src="{{ asset('assets/img/layout/simbolo-segmentoFarma.svg') }}" alt="Segmento Farma">
                        <h1>{!! $home->banner_titulo !!}</h1>
                    </div>                
                </div>
            </div>
        </div>

        <div class="bloco-2-home">
            <section class="container-fluid">
                <article class="row-grid">
                    <div class="col-12 col-md-12 col-sm-12 text-center">
                        <h1>{!! $home->titulo !!}</h1>
                    </div>
                </article>
                <article class="row-grid">
                    <div class="col-6 col-md-6 col-sm-12 p-2">
                        <img width="100%" src="{{ asset('assets/img/home/'.$home->imagem) }}" alt="Segmento Farma">
                    </div>
                    <div class="col-6 col-md-6 col-sm-12 pt-1">
                        {!! $home->descricao !!}
                    </div>
                </article>
            </section>
        </div>
    </div>

    <!-- bloco sobre -->
    <div id="sobre" class="container sobre">
        <section class="container-fluid">
            <div class="row-grid">
                <article class="col-12 col-md-12 col-sm-12 text-center">
                    <div class="background">
                        <h1>{!! $sobre->titulo !!}</h1>
                    </div>
                </article>
            </div>

            <div class="row-grid">
                <article class="col-12 col-md-12 col-sm-12 text-center">
                    <h3>{!! $sobre->sub_titulo !!}</h3>
                </article>
            </div>

            <div class="row-grid p-1 ">
                <article class="col-7 col-md-7 col-sm-12 content-with-image bloco-1">
                    <img src="{{ asset('assets/img/layout/historico-2020.svg') }}" alt="Segmento Farma">
                    <div class="pl-1 pl-sm-1 m-0">
                        {!! $sobre->descricao_1 !!}
                    </div>
                </article>
            </div>

            <div class="row-grid p-1">
                <div class="col-3 col-md-3 d-sm-none"></div>
                <article class="col-7 col-md-7 col-sm-12 mr-1 content-with-image bloco-2">
                    <img src="{{ asset('assets/img/layout/historico-1990.svg') }}" alt="Segmento Farma">
                    <div class="ml-1 content m-0 theme-color">
                        {!! $sobre->descricao_2 !!}
                    </div>
                </article>
            </div>

            <div class="row-grid p-1">
                <div class="col-5 col-md-5 d-sm-none"></div>
                <article class="col-7 col-md-7 col-sm-12 content-with-image bloco-1">
                    <img src="{{ asset('assets/img/layout/historico-2002.svg') }}" alt="Segmento Farma">
                    <div class="pl-1 pl-sm-1 m-0">
                        {!! $sobre->descricao_3 !!}
                    </div>
                </article>
            </div>

            @if ($sobre->documento)                
                <div class="row-grid p-1 documento">
                    <article class="col-12 col-md-12 col-sm-12  pt-1 content-with-image">
                        <div class="pr-1">
                            <p>Conheça nossa POLÍTICA EDITORAL:</p>
                        </div>
                        <div class="pl-sm-1">
                            <a href="{{ asset('assets/img/layout/sobre/'. $sobre->documento) }}" download="download"><button type="button" class="btn-pdf">Visualizar <img src="{{ asset('assets/img/layout/icone-PDF.svg') }}"></button></a>
                        </div>
                    </article>
                </div>
            @endif
        </section>
    </div>

    <!-- bloco serviços -->
    <div id="servicos" class="container servicos">
        <section class="container-fluid">
            <div class="row-grid">
                <article class="col-12 col-md-12 col-sm-12 text-center">
                    <div class="background">
                        <h1>{!! $servicos->titulo !!}</h1>
                    </div>
                </article>

                <article class="col-12 col-md-12 col-sm-12 text-center">
                    <div class="size-limited">
                        <p>{!! $servicos->sub_titulo !!}</p>
                    </div>
                </article>
            </div>

            <div class="row-grid mt-2">
                <div class="col-12 col-md-12 col-sm-12 text-center">
                    <div class="size-limited">
                        <h1 class="titulo">{!! $servicos->titulo_2 !!}</h1>
                        {!! $servicos->descricao !!}
                        <!-- video -->
                        <div class="video">                            
                            {!! $servicos->video !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-grid mt-3">
                <div class="col-12 col-md-12 col-sm-12 text-center">
                    <h1 class="titulo">{!! $servicos->titulo_3 !!}</h1>
                </div>
            </div>

            <div class="row-grid mt-2">
                <div class="col-4 col-md-4 col-sm-4 grid-impresos">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso1.jpg') }}" alt="Segmento Farma">
                </div>
                <div class="col-4 col-md-4 col-sm-4 grid-impresos">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso2.jpg') }}" alt="Segmento Farma">
                </div>
                <div class="col-4 col-md-4 col-sm-4 grid-impresos">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso3.jpg') }}" alt="Segmento Farma">
                </div>
            </div>

            <div style="margin-bottom:120px" class="size-limited bloco-menor">
                <div class="row-grid ">
                    <div class="col-5 col-md-5 col-sm-12">
                        <img class="img-absolute" src="{{ asset('assets/img/layout/conteudo-impresso4.jpg') }}" alt="Segmento Farma">
                    </div>
                    <div class="col-7 col-md-7 col-sm-12">
                        {!! $servicos->descricao_3 !!}
                        <img src="{{ asset('assets/img/layout/conteudo-impresso5.jpg') }}" alt="Segmento Farma">
                    </div>
                </div>
            </div>

            <div class="row-grid">
                <div class="col-4 col-md-4 col-sm-4 grid-impresos pb-0">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso6.jpg') }}" alt="Segmento Farma">
                </div>
                <div class="col-4 col-md-4 col-sm-4 grid-impresos pb-0">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso7.jpg') }}" alt="Segmento Farma">
                </div>
                <div class="col-4 col-md-4 col-sm-4 grid-impresos pb-0">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso8.jpg') }}" alt="Segmento Farma">
                </div>
                <div class="col-4 col-md-4 col-sm-4 grid-impresos pt-0">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso9.jpg') }}" alt="Segmento Farma">
                </div>
                <div class="col-4 col-md-4 col-sm-4 grid-impresos pt-0">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso10.jpg') }}" alt="Segmento Farma">
                </div>
                <div class="col-4 col-md-4 col-sm-4 grid-impresos pt-0">
                    <img src="{{ asset('assets/img/layout/conteudo-impresso11.jpg') }}" alt="Segmento Farma">
                </div>
            </div>

            <div class="size-limited bloco-menor">
                <div class="row-grid pl-2 pr-2">
                    <div class="col-12 col-md-12 col-sm-12 text-center img-ajusted">
                        <img src="{{ asset('assets/img/layout/conteudo-impresso12.png') }}" alt="Segmento Farma">
                    </div>
                    <div class="col-12 col-md-12 col-sm-12 text-center p-0">
                        <h4>{!! $servicos->titulo_4 !!}</h4>
                        {!! $servicos->descricao_4 !!}
                    </div>
                </div>
            </div>

            <div class="bloco-pequeno">
                <div class="row-grid pl-2 pr-2">
                    <div class="col-12 col-md-12 col-sm-12 text-center img-ajusted pt-0 pb-0 mb-0 mt-0">
                        <div class="row-grid">
                            <div class="col-md-12 content-span">
                                <p class="span">{!! $servicos->titulo_5 !!}</p>
                            </div>
                            <div class="col-2 col-md-4 d-sm-none "></div>
                            <div class="col-10 col-md-8 col-sm-12 pl-1 text-left">
                                {!! $servicos->descricao_5 !!}
                            </div>
                        </div>
                    </div>
                        <div class="col-12 col-md-12 col-sm-12 text-center p-0 m-0 mb-2 ">
                            <div class="row-grid">
                                <div class="col-4 col-md-4 col-sm-12 text-center">
                                    <img src="{{ asset('assets/img/layout/infografico1.jpg') }}" alt="Segmento Farma">
                                </div>
                                <div class="col-4 col-md-4 col-sm-12 text-center">
                                    <img src="{{ asset('assets/img/layout/infografico2.jpg') }}" alt="Segmento Farma">
                                </div>
                                <div class="col-4 col-md-4 col-sm-12 text-center">
                                    <img src="{{ asset('assets/img/layout/infografico3.jpg') }}" alt="Segmento Farma">
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- bloco parcerias -->

    <div id="parcerias" class="container parcerias">
        <section class="container-fluid">
            <article class="row-grid">
                <article class="col-12 col-md-12 col-sm-12 text-center">
                    <div class="background">
                        <h1>{!! $parcerias->titulo !!}</h1>
                        {!! $parcerias->descricao !!}
                    </div>
                </article>
            </article>
            <article class="row-grid">
                <?php $parceria = json_decode($parcerias->parcerias); ?>
                @if ($parceria)                    
                    @foreach ($parceria as $item)                    
                        <div class="col-4 col-md-4 col-sm-6 text-center">
                            <figure class="content-grid">
                                <img src="{{ asset('assets/img/parcerias/'.$item->foto) }}" alt="Segmento Farma">
                                <figcaption>{!! $item->descricao !!}</figcaption>
                            </figure>
                        </div>
                    @endforeach
                @endif
            </article>
        </section>
    </div>

    <!-- bloco clientes -->
    <div id="clientes" class="container clientes">
        <section class="container-fluid">
            <article class="d-flex p-0 m-0">
                <?php $cliente = json_decode($clientes->imagem); ?>
                @if ($cliente)                    
                    @foreach ($cliente as $item)                    
                        <div class="cliente">
                            <img src="{{ asset('assets/img/clientes/'.$item->foto) }}" alt="Segmento Farma clientes">
                        </div>
                    @endforeach
                @endif
            </article>
        </section>
    </div>

    <!-- fale conosco -->

    <div id="contato" class="contato">
        <section class="container-fluid">
            <div class="row-grid">
                <article class="col-5 col-md-5 col-sm-12 pt-0">
                    <h5>{!! $contato->titulo !!}</h5>
                    <p class="telefone">{!! $contato->telefone !!}</p>
                    <p class="endereco">{!! $contato->endereco !!}</p>
                </article>
                <article class="col-7 col-md-7 col-sm-12">
                    @if (session('success'))
                        <div class="flash flash-sucesso">
                            {!! session('success') !!}
                        </div>
                    @endif
                    <form action="{{ route('send-form') }}" method="post">
                        {{ csrf_field() }}                    
                        <div class="row-grid">
                            <div class="col-6 col-md-6 col-sm-12 p-0 m-0">
                                <input type="text" name="nome" placeholder="nome" />
                                <input type="email" name="email" placeholder="e-mail"/>
                                <input type="tel" name="telefone" placeholder="telefone" />
                            </div>
                            <div style="padding: 0 0 0 2px" class="col-6 col-md-6 col-sm-12 m-0">
                                <textarea name="mensagem" placeholder="mensagem"></textarea>
                            </div>
                        </div>
                        <div class="row-grid text-right p-0 m-0">
                            <div class="col-12 col-md-12 col-sm-12 p-0 m-0">
                                <button type="submit" class="submit">enviar </button>
                            </div>
                        </div>
                    </form>
                </article>
            </div>
        </section>
        <section class="container mt-2">
            <div class="google-maps">
                {!! $contato->google_maps !!}
            </div>
        </section>
    </div>
@endsection