    <header>
        <div class="container">
            <div class="container-fluid header-main">
                <div class="row-grid">
                    <div class="col-3 col-md-4 col-sm-8 p-0">
                        <div class="logo-menu">
                            <a href="{{ route('home') }}" class="logo"></a>
                        </div>
                    </div>
                    <div class="col-9 col-md-4 p-0 container-nav text-right">
                        <nav id="nav-desktop">
                            @include('frontend.common.nav')
                        </nav>
                    </div>
                </div>
                
                <button id="mobile-toggle">
                    <div class="lines"></div>
                </button>
            </div>
        </div>
    </header>

    <nav id="nav-mobile">
        <div class="center">
            @include('frontend.common.nav')
        </div>
    </nav>
