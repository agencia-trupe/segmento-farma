<div>
    <a href="#sobre" class="nav-link ">
        Sobre
    </a>
</div>
<div>
    <a href="#servicos" class="nav-link">
        Serviços
    </a>
</div>
<div>
    <a href="#parcerias" class="nav-link">
        Parcerias
    </a>
</div>
<div>
    <a href="#clientes" class="nav-link">
        Clientes
    </a>
</div>
<div>
    <a href="#contato" class="nav-link ">
        Contato
    </a>
</div>
<div class="redes-sociais">
    <ul class="ul-inline">
        <li><a href="{!! $contato->facebook !!}" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="Facebook Segmento Farma"></a></li>
        <li><a href="{!! $contato->instagram !!}" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="Instagram Segmento Farma"></a></li>
    </ul>
</div>
