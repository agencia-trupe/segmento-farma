    <footer class="container">
        <div class="container-fluid">
            <div class="row-grid">
                <div class="col-2 col-md-2 col-sm-12 p-0">
                    <img class="logo-footer" src="{{ asset('assets/img/layout/marca-SegmentoFarma.svg') }}" alt="Segmento Farma">
                </div>
                <div class="col-2 col-md-2 col-sm-6">
                    <span class="telefone"><strong>11 3093 3300</strong></span>
                    <p class="endereco">Rua Anseriz • Campo Belo 04618-050 • São Paulo • SP</p>
                    <ul class="ul-inline p-0 text-left">
                        <li><a href="{!! $contato->facebook !!}" target="_blank"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="Facebook Segmento Farma"></a></li>
        <li><a href="{!! $contato->instagram !!}" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="Instagram Segmento Farma"></a></li>
                    </ul>
                </div>
                <div class="col-2 col-md-2 col-sm-6">
                    <ul class="menu-footer">
                        <li><a href="#sobre" class="nav-link">sobre</a></li>
                        <li><a href="#servicos" class="nav-link">serviços</a></li>
                        <li><a href="#parcerias" class="nav-link">parcerias</a></li>
                        <li><a href="#clientes" class="nav-link">clientes</a></li>
                        <li><a href="#contato" class="nav-link">contato</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md-6 col-sm-12">
                    @if (session('success'))
                        <div class="flash flash-sucesso">
                            {!! session('success') !!}
                        </div>
                    @endif
                    <form action="{{ route('send-form') }}" method="post">
                        {{ csrf_field() }}
                        <p>Fale com a Segmento Farma:</p>
                        <div class="row-grid">
                            <div class="col-6 col-md-6 col-sm-12 p-0 m-0">
                                <input type="text" name="nome" placeholder="nome" />
                                <input type="email" name="email" placeholder="e-mail"/>
                                <input type="tel" name="telefone" placeholder="telefone" />
                            </div>
                            <div style="padding: 0 0 0 2px" class="col-6 col-md-6 col-sm-12 m-0">
                                <textarea name="mensagem" placeholder="mensagem"></textarea>
                            </div>
                        </div>
                        <div class="row-grid text-right p-0 m-0">
                            <div class="col-12 col-md-12 col-sm-12 p-0 m-0">
                                <button type="submit" class="submit">enviar </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="row-grid copy">
            <div class="col-12 col-md-12 col-sm-12 text-center p-0 m-0">
                <p><a href="{{ route('politica-de-privacidade') }}">POLÍTICA DE PRIVACIDADE</a> | &copy 2021 Segmento Farma • Todos os direitos reservados.</p>
            </div>
        </div>
    </footer>
