@include('painel.common.flash')

<div class="form-row">
    <div class="form-group col-md-6 col-sm-12">
        {!! Form::label('telefone', 'Telefone') !!}
        {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6 col-sm-12" >
        {!! Form::label('whatsapp', 'Whatsapp') !!}
        {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-row">
    <div class="form-group col-md-12">
        {!! Form::label('endereco', 'Endereço') !!}
        {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-4 col-sm-12">
        {!! Form::label('link_youtube', 'Link Youtube') !!}
        {!! Form::text('link_youtube', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4 col-sm-12" >
        {!! Form::label('link_linkedin', 'Link linkedin') !!}
        {!! Form::text('link_linkedin', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4 col-sm-12" >
        {!! Form::label('link_instagram', 'Link Instagram') !!}
        {!! Form::text('link_instagram', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr>

<div class="form-row">
    <div class="form-group col-md-12">
        {!! Form::label('google_maps', 'Google Maps') !!}
        {!! Form::textarea('google_maps', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-12">
        {!! Form::label('email_fale_conosco', 'Email de contato') !!}
        {!! Form::text('email_fale_conosco', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-row">
    <div class="form-group col-md-12">
        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
    </div>
</div>
