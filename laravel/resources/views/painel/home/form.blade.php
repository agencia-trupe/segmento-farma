@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('banner', 'Imagem do banner') !!}
@if($submitText == 'Alterar' && $registro->banner)
    <img src="{{ url('assets/img/home/'.$registro->banner) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('banner', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('banner_titulo', 'Banner Título') !!}
    {!! Form::text('banner_titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título 2') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('imagem', 'Imagem') !!}
        <img src="{{ url('assets/img/home/'.$registro->imagem ?: null) }}" style="display:block; margin-bottom: 10px; max-width: 30%;">
    {!! Form::file('imagem', null, ['class' => 'form-control']) !!}
</div> 

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
