@include('painel.common.flash')

<div class="form-group text-right">
    <button type="button" class="btn btn-info add_field_button">Adicionar parceiros</button>
</div>
<div id="addItemCarouselClientes" class="well form-group">
    <?php
        $imagem = json_decode($registro->imagem);  
    ?>
    @if (isset($imagem))
    @foreach ($imagem as $key => $item)     
    <div class="row addItemCarouselClientes">
            <div class="col-md-4">
                <img src="{{ url('assets/img/clientes/'.$item->foto) }}" style="display:block; margin-bottom: 10px; max-width: 40%;">
            </div>
            <div class="col-md-4">
                <input type="file" name="carrossel[{{ $key  }}][foto]" value="{{ $item->foto }}" class="form-control">
                <input type="text" name="carrossel[{{ $key  }}][isFile]" class="btn btn-info btn-sm btn-block pointer-events" value="{{ $item->foto }}"><br>
            </div>
            <div class="col-md-4 text-right">            
                <button class="btn btn-danger remove_field" type="button">Remover</button>
            </div>
        </div> <hr>
    @endforeach
    @endif        
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
