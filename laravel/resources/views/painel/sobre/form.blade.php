@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('sub_titulo', 'Sub Título') !!}
    {!! Form::text('sub_titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_1', 'Descrição 1') !!}
    {!! Form::textarea('descricao_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_2', 'Descrição 2') !!}
    {!! Form::textarea('descricao_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_3', 'Descrição 3') !!}
    {!! Form::textarea('descricao_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
</div>

<div class="form-group">
    {!! Form::label('documento', 'Política Editoral') !!}
    {!! Form::file('documento', null, ['class' => 'form-control']) !!}
    @if ($registro->documento)        
        <a href="{{ url('assets/img/sobre/'.$registro->documento ?: null) }}">>> Ver anexo</a>
    @endif
</div> 

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
