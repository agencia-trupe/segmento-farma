@extends('painel.common.template')

@section('content')

<legend>
    <h2>Política de privacidade</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.politica-de-privacidade.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.politica-de-privacidade.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection