@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('sub_titulo', 'Sub Título') !!}
    {!! Form::text('sub_titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_2', 'Título 2') !!}
    {!! Form::text('titulo_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('sub_titulo_2', 'Sub Título 2') !!}
    {!! Form::text('sub_titulo_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Iframe vídeo') !!}
    <small><font color="red">** Insira apenas o iframe do vídeo</font></small>
    {!! Form::textarea('video', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_3', 'Título 3') !!}
    {!! Form::text('titulo_3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_3', 'Descrição 3') !!}
    {!! Form::textarea('descricao_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_4', 'Título 4') !!}
    {!! Form::text('titulo_4', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_4', 'Descrição 4') !!}
    {!! Form::textarea('descricao_4', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_5', 'Título 5') !!}
    {!! Form::text('titulo_5', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_5', 'Descrição 5') !!}
    {!! Form::textarea('descricao_5', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
