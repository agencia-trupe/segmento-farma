<ul class="nav navbar-nav">
    <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
        <a href="{{ route('painel.home.index') }}">Home</a>
    </li>
    <li @if(Tools::routeIs('painel.sobre*')) class="active" @endif>
        <a href="{{ route('painel.sobre.index') }}">Sobre</a>
    </li>
    <li @if(Tools::routeIs('painel.servicos*')) class="active" @endif>
        <a href="{{ route('painel.servicos.index') }}">Serviços</a>
    </li>
    <li @if(Tools::routeIs('painel.parcerias*')) class="active" @endif>
        <a href="{{ route('painel.parcerias.index') }}">Parcerias</a>
    </li>
    <li @if(Tools::routeIs('painel.clientes*')) class="active" @endif>
        <a href="{{ route('painel.clientes.index') }}">Clientes</a>
    </li>
    <li @if(Tools::routeIs('painel.contato*')) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Contato</a>
    </li>
</ul>