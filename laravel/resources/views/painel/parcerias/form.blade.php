@include('painel.common.flash')

<div class="form-group">
        {!! Form::label('titulo', 'Título') !!}
        {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
        {!! Form::label('descricao', 'Descrição') !!}
        {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="form-group text-right">
    <button type="button" class="btn btn-info add_field_button">Adicionar parceiros</button>
</div>
<div id="addItemCarouselHome" class="well form-group">
    <?php
        $parcerias = json_decode($registro->parcerias);  
    ?>
    @if (isset($parcerias))
    @foreach ($parcerias as $key => $item)     
    <div class="row addItemCarouselHome">
            <div class="col-md-5">
                <label>Descrição</label>
                <input type="text" name="carrossel[{{ $key  }}][descricao]" value="{!! $item->descricao !!}" class="form-control">
            </div>
            <div class="col-md-5">
                <img src="{{ url('assets/img/parcerias/'.$item->foto) }}" style="display:block; margin-bottom: 10px; max-width: 30%;">
                <input type="file" name="carrossel[{{ $key  }}][foto]" value="{{ $item->foto }}" class="form-control">
                <input type="text" name="carrossel[{{ $key  }}][isFile]" class="btn btn-info btn-sm btn-block pointer-events" value="{{ $item->foto }}"><br>
            </div>
            <div class="col-md-2">            
                <button class="btn btn-danger remove_field" type="button">Remover</button>
            </div>
        </div> 
    @endforeach
    @endif        
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
