@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Parcerias</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.parcerias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.parcerias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
