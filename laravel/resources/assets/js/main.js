import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.href + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
          $(".aviso-cookies").hide();
        },
      });
    });
  }

  //scroll suave nas ancoras do menu
  var $doc = $("html, body");
  $(".nav-link").click(function () {
    $doc.animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top - 100,
      },
      600
    );
    return false;
  });

  //active menu
  var btns = $(".nav-link");
  btns.click(function () {
    var active = $(".active");
    active.first().removeClass("active");
    $(this).addClass("active");
  });

  // fechar nav-mobile
  $("#nav-mobile .center a").click(function () {
    $("#nav-mobile").css("display", "none");
    $("#mobile-toggle").removeClass("close");
  });
});
