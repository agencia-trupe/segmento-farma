<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table){
            $table->increments('id');
            $table->text('titulo')->nullable();
            $table->text('sub_titulo')->nullable();
            $table->text('titulo_2')->nullable();
            $table->text('sub_titulo_2')->nullable();
            $table->text('video')->nullable();
            $table->text('titulo_3')->nullable();
            $table->text('descricao_3')->nullable();
            $table->text('titulo_4')->nullable();
            $table->text('descricao_4')->nullable();
            $table->text('titulo_5')->nullable();
            $table->text('descricao_5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicos');
    }
}
