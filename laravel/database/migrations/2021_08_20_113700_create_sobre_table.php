<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sobre', function (Blueprint $table){
            $table->increments('id');
            $table->text('titulo')->nullable();
            $table->text('sub_titulo')->nullable();
            $table->text('descricao_1')->nullable();
            $table->text('descricao_2')->nullable();
            $table->text('descricao_3')->nullable();
            $table->text('documento')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sobre');
    }
}
